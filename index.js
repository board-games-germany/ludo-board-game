const express = require('express'),
    app = express(),
    server = require('http').createServer(app);

// app.use('/', function (req, res) {
//     res.sendFile(__dirname + '/client/index.html');
// });

app.use('/', express.static(__dirname + '/client'));

server.listen(8000);
console.log('server started');


const io = require('socket.io')(server, {});

io.sockets.on('connection', function(socket){
    console.log('socket connection');

    socket.on('happy', function(data){
        console.log('client is happy', data);
    })

    socket.emit('serverMsg', {
        msg: 'this is a message out of the dark'
    })
});



